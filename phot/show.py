import asyncio

class Slideshow:
  def __init__(self, config):
    self.config = config
    self.params = "--noclear --smartfit 30 --delay 1"

  async def show(self, filename):
    if filename is None:
      filename = self.config['default_image']
    print(filename)
    await asyncio.create_subprocess_shell('fbv {params} "{image}"'.format(params=self.params, image=filename))
    await asyncio.sleep(self.config['interval'])
