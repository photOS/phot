import logging
logging.basicConfig(level=logging.INFO)
logging.getLogger("phot").setLevel(logging.DEBUG)

logger = logging.getLogger()


import asyncio
from . import show

from .config import config
from . import select
from .plugins import webdav
running=True


slideshow = show.Slideshow(config['slideshow'])

async def stop():
  await asyncio.sleep(45)
  print("Stopping")
  global running
  runnning=False

async def run():
  mediaprovider = webdav.MediaProvider(config['webdav'], patterns=["*.jpg", "*.JPG", "*.png", "*.PNG"])
  #asyncio.create_task(stop())
  if "shuffle" in config["slideshow"] and config["slideshow"]["shuffle"] is False:
    selector=select.Select(mediaprovider)
  else:
    selector=select.SelectRandom(mediaprovider)
  
  while(running):
    await slideshow.show(selector.get_next())


def main():
  asyncio.run(run())
