import asyncio

import logging
logger = logging.getLogger(__name__)

import glob
import os

from pathlib import Path

from os import listdir
from os.path import isfile, join

class MediaProvider:
  def __init__(self, config, patterns=[]):
    self.config=config
    self.filepatterns=patterns
    Path(self.config['folder']).mkdir(parents=True, exist_ok=True)
    self.sync_condition = asyncio.Condition()
    asyncio.create_task(self.regular_sync())

  def get_list(self):
    path = Path(self.config['folder'])
    files = path.glob("*")
    onlyfiles = [x for x in files if x.is_file()]
    return sorted(onlyfiles)
  
  def createCommand(self):
    cmd=["rclone"]
    cmd.append("sync")
    cmd.append("--webdav-url={url}".format(url=self.config['url']))
    cmd.append("--webdav-vendor={vendor}".format(vendor=self.config['vendor']))
    cmd.append("--webdav-user={user}".format(user=self.config['user']))
    cmd.append("--webdav-pass={password}".format(password=self.config['password']))
    for pattern in self.filepatterns:
      cmd.append("--include={pattern}".format(pattern=pattern))
    if "certificate_check" in self.config and self.config["certificate_check"] is False:
      cmd.append("--no-check-certificate")
    if "ca_cert" in self.config:
      cmd.append("--ca-cert {filepath}".format(filepath=self.config["ca_cert"]))
    
    cmd.append(":webdav:")
    cmd.append("{folder}".format(folder=self.config['folder']))
    return cmd
  
  
  async def sync(self):
    async with self.sync_condition:
      cmd=self.createCommand()
      logger.info("Starting sync")
      logger.debug("rclone parameters: {cmd}".format(cmd=cmd))
      await asyncio.create_subprocess_exec(*cmd)
      logger.info("Sync finished")

  async def regular_sync(self):
    while True:
      await self.sync()
      await asyncio.sleep(self.config['interval']*60)
    


  async def obscure_password(self, password):
    # https://rclone.org/commands/rclone_obscure/
    cmd = ["rclone"]
    cmd.append("obscure")
    cmd.append("{password}".format(password=password))
    proc = await asyncio.create_subprocess_exec(*cmd, stdout=asyncio.subprocess.PIPE)

    stdout, stderr = await proc.communicate()
    obscured_password=stdout.decode().rstrip()
    return obscured_password
