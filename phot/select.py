import logging
logger = logging.getLogger(__name__)


import random
from collections import deque

def filter_supported(file):
  suffix=file.suffix
  if (suffix == ".jpg"):
    return True
  
  return False

class Select:
  """Selects files from a folder"""

  def __init__(self, mediaprovider):
    self.index=-1
    self.provider=mediaprovider

  def get_file_list(self):
    return list(filter(filter_supported, self.provider.get_list()))

  def get_next(self):
    self.index = self.index + 1
    files = self.get_file_list()
    if (len(files)==0):
      # No files available
      self.index = -1
      return None
    
    self.index=self.index % len(files)
    return files[self.index]

  def get_previous(self):
    self.index=self.index-2
    return self.get_next()


class SelectRandom(Select):
  """Selects files from a folder"""

  def __init__(self, mediaprovider):
    self.history={}
    self.history["past"]=deque(maxlen=10)
    self.history["future"]=deque(maxlen=10)
    self.history_index=-1
    self.provider=mediaprovider

  def get_next(self):
    files = self.get_file_list()
    amount_files=len(files)
    if (amount_files==0):
      # No files available
      return None

    if len(self.history["future"])>0:
      newindex=self.history["future"].pop()
    else:
      newindex=random.randrange(amount_files)

    self.history["past"].append(newindex)
    
    return files[newindex]

  def get_previous(self):
    files = self.get_file_list()
    amount_files=len(files)
    if (amount_files==0):
      # No files available
      return None

    if len(self.history["past"])>0:
      newindex=self.history["past"].pop()
    else:
      newindex=random.randrange(amount_files)
    
    self.history["future"].append(newindex)

    return files[newindex]
