#! /usr/bin/env python

import logging
logging.basicConfig(level=logging.INFO)
logging.getLogger("phot").setLevel(logging.DEBUG)

logger = logging.getLogger()

import unittest
from unittest.mock import patch

import sys
sys.path.insert(1, '../src')
from phot import select


file_list=["File_1", "File_2", "File_3", "File_4"]
class TestBasicSelection(unittest.TestCase):
  
  @patch.object(select.Select, 'get_file_list')
  def test_no_files(self, get_file_list_mock):
    get_file_list_mock.return_value = []
    selector = select.Select("somewhere")
    self.assertIsNone(selector.get_next())

  @patch.object(select.Select, 'get_file_list')
  def test_get_next(self, get_file_list_mock):
    get_file_list_mock.return_value = file_list
    selector = select.Select("somewhere")
    self.assertEqual(selector.get_next(), file_list[0])

  @patch.object(select.Select, 'get_file_list')
  def test_get_next_overflow(self, get_file_list_mock):
    get_file_list_mock.return_value = file_list
    selector = select.Select("somewhere")
    selector.index=3
    self.assertEqual(selector.get_next(), file_list[0])

  @patch.object(select.Select, 'get_file_list')
  def test_get_previous(self, get_file_list_mock):
    get_file_list_mock.return_value = file_list
    selector = select.Select("somewhere")
    selector.index=1
    self.assertEqual(selector.get_previous(), file_list[0])

  @patch.object(select.Select, 'get_file_list')
  def test_get_previous_underflow(self, get_file_list_mock):
    get_file_list_mock.return_value = file_list
    selector = select.Select("somewhere")
    selector.index=0
    self.assertEqual(selector.get_previous(), file_list[3])



class RandomSelection(unittest.TestCase):
  
  @patch.object(select.Select, 'get_file_list')
  def test_no_files(self, get_file_list_mock):
    get_file_list_mock.return_value = []
    selector = select.SelectRandom("somewhere")
    self.assertIsNone(selector.get_next())

  @patch.object(select.Select, 'get_file_list')
  def test_get_previous(self, get_file_list_mock):
    get_file_list_mock.return_value = file_list
    selector = select.SelectRandom("somewhere")
    
    n1=selector.get_next()
    n2=selector.get_next()
    
    p1=selector.get_previous()
    self.assertEqual(p1, n1)
    p2=selector.get_previous()
    p3=selector.get_previous()
    n2=selector.get_next()
    self.assertEqual(p2, n2)
    n1=selector.get_next()
    self.assertEqual(p1, n1)
    



if __name__ == '__main__':
  unittest.main()
